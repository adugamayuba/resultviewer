package com.resultchecker;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

public class NoobChain {

    public static ArrayList<Block> blockchain = new ArrayList<Block>();
    public static int difficulty = 5;

    public static void main(String[] args) {
        //add our blocks to the blockchain ArrayList:

        blockchain.add(new Block("Hi im the first block", "0"));
        System.out.println("Trying to Mine block 1... ");
        blockchain.get(0).mineBlock(difficulty);

        blockchain.add(new Block("Yo im the second block",blockchain.get(blockchain.size()-1).hash));
        System.out.println("Trying to Mine block 2... ");
        blockchain.get(1).mineBlock(difficulty);

        blockchain.add(new Block("Hey im the third block",blockchain.get(blockchain.size()-1).hash));
        System.out.println("Trying to Mine block 3... ");
        blockchain.get(2).mineBlock(difficulty);

        System.out.println("\nBlockchain is Valid: " + isChainValid());

        String blockchainJson = new GsonBuilder().setPrettyPrinting().create().toJson(blockchain);
        System.out.println("\nThe block chain: ");
        System.out.println(blockchainJson);
    }

    public static Boolean isChainValid() {
        Block currentBlock;
        Block previousBlock;
        String hashTarget = new String(new char[difficulty]).replace('\0', '0');

        //loop through blockchain to check hashes:
        for(int i=1; i < blockchain.size(); i++) {
            currentBlock = blockchain.get(i);
            previousBlock = blockchain.get(i-1);
            //compare registered hash and calculated hash:
            if(!currentBlock.hash.equals(currentBlock.calculateHash()) ){
                System.out.println("Current Hashes not equal");
                return false;
            }
            //compare previous hash and registered previous hash
            if(!previousBlock.hash.equals(currentBlock.previousHash) ) {
                System.out.println("Previous Hashes not equal");
                return false;
            }
            //check if hash is solved
            if(!currentBlock.hash.substring( 0, difficulty).equals(hashTarget)) {
                System.out.println("This block hasn't been mined");
                return false;
            }
        }
        return true;
    }
}
public class ViewUploadsActivity extends AppCompatActivity {

    public static TextView textDis;

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    //   DatabaseReference d = com.google.firebase.database.DatabaseReference.goOnline();
    private DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_uploads);














        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("studentid");

        // Set up the input
        final EditText input = new EditText(this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT );
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String   mm_Text = input.getText().toString();
                final String m_Text = mm_Text;
//                if(mm_Text.isEmpty()){
//
//                }

                mDatabase = FirebaseDatabase.getInstance().getReference();
                //  String key = mDatabase.child("user").push().getKey();

                String uemailinput = m_Text;

                DocumentReference user = db.collection("users").document(uemailinput);
                user.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot docname = task.getResult();
                            StringBuilder fieldsNAME = new StringBuilder("");
                            fieldsNAME.append("").append(docname.get("FIRSTGRADE"));
                            TextView textDisplayname = findViewById(R.id.textView5);
                            textDisplayname.setText(fieldsNAME.toString());


                            DocumentSnapshot SUBJECTONE = task.getResult();
                            StringBuilder FIRSTSUBJECT = new StringBuilder("");
                            FIRSTSUBJECT.append("").append(SUBJECTONE.get("FIRSTSUBJECT"));
                            TextView firstsub = findViewById(R.id.textView21);
                            firstsub.setText(FIRSTSUBJECT.toString());

                            // String amprise = textDisplay.getText().toString();


                            DocumentSnapshot docemail = task.getResult();
                            StringBuilder fieldsemail = new StringBuilder("");
                            fieldsemail.append("").append(docemail.get("SECONDGRADE"));
                            TextView   textDisplayemail = findViewById(R.id.textView18);
                            textDisplayemail.setText(fieldsemail.toString());

                            DocumentSnapshot qSUBJECTONE = task.getResult();
                            StringBuilder qFIRSTSUBJECT = new StringBuilder("");
                            qFIRSTSUBJECT.append("").append(qSUBJECTONE.get("SECONDSUBJECT"));
                            TextView qfirstsub = findViewById(R.id.textView22);
                            qfirstsub.setText(qFIRSTSUBJECT.toString());














                            DocumentSnapshot doccontact = task.getResult();
                            StringBuilder fieldscontact = new StringBuilder("");
                            fieldscontact.append("").append(doccontact.get("THIRDGRADE"));
                            TextView   textDisplaycontact = findViewById(R.id.textView19);
                            textDisplaycontact.setText(fieldscontact.toString());





                            DocumentSnapshot vSUBJECTONE = task.getResult();
                            StringBuilder vFIRSTSUBJECT = new StringBuilder("");
                            vFIRSTSUBJECT.append("").append(vSUBJECTONE.get("THIRDSUBJECT"));
                            TextView vfirstsub = findViewById(R.id.textView23);
                            vfirstsub.setText(vFIRSTSUBJECT.toString());


















                            DocumentSnapshot docpayamount = task.getResult();
                            StringBuilder fieldspayamount = new StringBuilder("");
                            fieldspayamount.append("").append(docpayamount.get("FOURTHGRADE"));
                            TextView   textDisplayloanamount = findViewById(R.id.textView20);
                            textDisplayloanamount.setText(fieldspayamount.toString());


                            DocumentSnapshot mSUBJECTONE = task.getResult();
                            StringBuilder mFIRSTSUBJECT = new StringBuilder("");
                            mFIRSTSUBJECT.append("").append(mSUBJECTONE.get("FOURTHSUBJECT"));
                            TextView mfirstsub = findViewById(R.id.textView24);
                            mfirstsub.setText(mFIRSTSUBJECT.toString());



                        }
                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                            }
                        });

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent ex = new Intent();


                finish();

            }
        });

        builder.show();

















    }



    private void ReadSingleContact() {


        mDatabase = FirebaseDatabase.getInstance().getReference();
        String   key = mDatabase.child("user").push().getKey();


        DocumentReference user = db.collection("user").document(key);
        user.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task< DocumentSnapshot > task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot doc = task.getResult();
                    StringBuilder fields = new StringBuilder("");
                    fields.append("").append(doc.get("Interest Pay Amount"));
                    TextView textDisplayname = findViewById(R.id.textView5);
                    textDisplayname.setText(fields.toString());
                    // String amprise = textDisplay.getText().toString();


                    DocumentSnapshot docemail = task.getResult();
                    StringBuilder fieldsemail = new StringBuilder("");
                    fieldsemail.append("").append(docemail.get("Interest Pay Amount"));
                    TextView textDisplayemail = findViewById(R.id.textView18);
                    textDisplayemail.setText(fields.toString());

                    DocumentSnapshot doccontact = task.getResult();
                    StringBuilder fieldscontact = new StringBuilder("");
                    fieldscontact.append("").append(doccontact.get("Interest Pay Amount"));
                    TextView   textDisplaycontact = findViewById(R.id.textView19);
                    textDisplaycontact.setText(fields.toString());

                    DocumentSnapshot docpayamount = task.getResult();
                    StringBuilder fieldspayamount = new StringBuilder("");
                    fieldspayamount.append("").append(docpayamount.get("Interest Pay Amount"));
                    TextView   textDisplayloanamount = findViewById(R.id.textView20);
                    textDisplayloanamount.setText(fields.toString());




                }
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });
    }
}
//
//
//    //the listview
//    ListView listView;
//
//    //database reference to get uploads data
//    DatabaseReference mDatabaseReference;
//
//    //list to store uploads data
//    List<Upload> uploadList;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_view_uploads);
//        MultiDex.install(this);
//        uploadList = new ArrayList<>();
//        listView = (ListView) findViewById(R.id.listView);
//
//
//        //adding a clicklistener on listview
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                //getting the upload
//                Upload upload = uploadList.get(i);
//
//                //Opening the upload file in browser using the upload url
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//                intent.setData(Uri.parse(upload.getUrl()));
//                startActivity(intent);
//            }
//        });
//
//
//        //getting the database reference
//        mDatabaseReference = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_UPLOADS);
//
//        //retrieving upload data from firebase database
//        mDatabaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
//                    Upload upload = postSnapshot.getValue(Upload.class);
//                    uploadList.add(upload);
//                }
//
//                String[] uploads = new String[uploadList.size()];
//
//                for (int i = 0; i < uploads.length; i++) {
//                    uploads[i] = uploadList.get(i).getName();
//                }
//
//                //displaying it to list
//                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, uploads);
//                listView.setAdapter(adapter);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//    }
//
//
//}