package com.resultchecker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Adminlogin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adminlogin);


        Button submit = findViewById(R.id.adminsubmit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                EditText edtusername = findViewById(R.id.username);
                String username = edtusername.getText().toString();

                EditText edtpassword = findViewById(R.id.password);
                String password = edtpassword.getText().toString();

                if(username.equals("admin") && password.equals("password")){


                    Intent adminactivity = new Intent();
                    adminactivity.setClass(Adminlogin.this, MainActivity.class);
                    startActivity(adminactivity);
                    finish();
                }else{
                    finish();
                }


            }
        });

    }
}
