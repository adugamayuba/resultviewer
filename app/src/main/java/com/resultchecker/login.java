package com.resultchecker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        Button admin = findViewById(R.id.adminbtn);

        admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent adminactivity = new Intent();
                adminactivity.setClass(login.this, Adminlogin.class);
                startActivity(adminactivity);

            }
        });





        Button student = findViewById(R.id.studentbtn);

        student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent adminactivity = new Intent();
                adminactivity.setClass(login.this, ViewUploadsActivity.class);
                startActivity(adminactivity);
            }
        });





    }
}
